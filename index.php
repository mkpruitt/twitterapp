<!DOCTYPE html>
<html lang="en">
  <head>
  </head>
  <body>
    <?php
    // Used https://code.tutsplus.com/tutorials/how-to-authenticate-users-with-twitter-oauth-20--cms-25713
    // as a reference
    
    require_once "\\twitteroauth\\autoload.php"; //Path to twitteroauth library
    use Abraham\TwitterOAuth\TwitterOAuth;
    
    session_start();
    $config = require_once "config.php";

    $conn = new TwitterOAuth($config['consumer_key'], $config['consumer_secret']);
    $token = $conn->oauth('oauth/request_token', ['oauth_callback' => $config['url_callback']]);
    
    // throw exception if something gone wrong
    if($conn->getLastHttpCode() != 200) {
        throw new \Exception('There was a problem connecting to twitter');
    }
     
    // save token of application to session
    $_SESSION['oauth_token'] = $token['oauth_token'];
    $_SESSION['oauth_token_secret'] = $token['oauth_token_secret'];
    if (isset($_POST["search_tag"])) { 
      $_SESSION['search_tag'] = $_POST["search_tag"]; 
    } else if (!isset($_SESSION["search_tag"])) {
      $_SESSION['search_tag'] = $config["default_tag"];
    }
    if (isset($_POST['like_id'])) { 
      $_SESSION['like_id'] = $_POST['like_id']; 
    }
    if (isset($_POST['unlike_id'])) { 
      $_SESSION['unlike_id'] = $_POST['unlike_id']; 
    }
     
    // generate the URL to make request to authorize our application
    $url = $conn->url('oauth/authorize', ['oauth_token' => $token['oauth_token']]);
     
    // and redirect
    header('Location: '. $url);
    ?>
    </div>
  </body>
</html>