Note that the server was set up to run on port 8080, if another port is desired
change the $port variable in 'config.php'.

The app uses the TwitterOAuth php library (https://twitteroauth.com/) as well
as the tablesorter javascript library (https://mottie.github.io/tablesorter/docs/),
which I did not write.

The app pulls data from the twitter rest api (https://dev.twitter.com/rest/public).