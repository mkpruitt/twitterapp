<!DOCTYPE html>
<html lang="en">
  <head>
    <?php session_start() ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="jquery.tablesorter.min.js"></script>
    <script src="jquery.tablesorter.pager.js"></script>
    <script>
      $(document).ready(function() {
        $("#tweetTable").tablesorter({
          theme: "blue",
          widgets: ["zebra"],
        }).tablesorterPager({
          container: $(".pager"),
          fixedHeight: true,
          
          cssFirst: '.first',
          cssPrev: '.prev',
          cssNext: '.next',
          cssLast: '.last',
          cssPageDisplay: '.pagedisplay',
          cssPageSize: '.pagesize',
        });
      });
    </script>
  </head>
  <body>
    <div>
      <form method="post" action="index.php">
        <input type="text" name="search_tag" value=<?php echo '"'.$_SESSION['search_tag'].'"' ?>>
        <input type="submit" name="submit" value="Search">
      </form>
    </div>
    
    <!-- pager -->
    <div id="pager" class="pager">
      <form>
        <img src="first.png" class="first"/>
        <img src="prev.png" class="prev"/>
        <!-- the "pagedisplay" can be any element, including an input -->
        <span class="pagedisplay"></span>
        <img src="next.png" class="next"/>
        <img src="last.png" class="last"/>
        <select class="pagesize">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
        </select>
      </form>
    </div>
    
    <table id="tweetTable" border="0" cellpadding="5" cellspacing="0">
      <thead>
        <tr>
          <th>Picture</th>
          <th>Author</th>
          <th>Time</th>
          <th>Content</th>
          <th>Retweets</th>
          <th>Likes</th>
          <th>Like</th>
        </tr>
      </thead>
      <tbody>
      <?php
      require_once '\\twitteroauth\\autoload.php';
      use Abraham\TwitterOAuth\TwitterOAuth;

      $config = require_once 'config.php';

      // Get and filter oauth verifier
      $oauth_verifier = filter_input(INPUT_GET, 'oauth_verifier');

      // Check tokens and redirect if login data is missing
      if (empty($oauth_verifier) ||
          empty($_SESSION['oauth_token']) ||
          empty($_SESSION['oauth_token_secret'])) {
        header('Location: ' . $config['url_login']);
      }

      // Connect with application token
      $conn = new TwitterOAuth($config['consumer_key'], $config['consumer_secret'],
                               $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

      // Request user token
      $token = $conn->oauth('oauth/access_token', ['oauth_verifier' => $oauth_verifier]);

      // Connect with user token
      $twitter = new TwitterOAuth($config['consumer_key'], $config['consumer_secret'],
                                  $token['oauth_token'], $token['oauth_token_secret']);
      $user = $twitter->get('account/verify_credentials');
      
      // Check for an error to return to log in
      if(isset($user->error)) {
        header('Location: ' . $config['url_login']);
      }
      
      // Post favorite update
      if(isset($_SESSION['like_id'])) {
         $twitter->post('favorites/create', ['id' => $_SESSION['like_id']]);
      }
      if(isset($_SESSION['unlike_id'])) {
         $twitter->post('favorites/destroy', ['id' => $_SESSION['unlike_id']]);
      }
      
      $tweets = $twitter->get("search/tweets", ["q" => "#".$_SESSION["search_tag"], "count" => "100"]);
      foreach($tweets as $tweet_set) {
        foreach($tweet_set as $tweet) {
          // Check if object has values set
          if (isset($tweet->id) && isset($tweet->user) && isset($tweet->created_at)
          && isset($tweet->text) && isset($tweet->retweet_count) && isset($tweet->favorite_count)){
            echo '<tr>';
            echo '<td><image src="'.$tweet->user->profile_image_url.'" /></td>'; // Picture
            echo '<td>'.$tweet->user->screen_name.'</td>'; // Author
            echo '<td>'.$tweet->created_at.'</td>'; // Time
            echo '<td>'.$tweet->text.'</td>'; // Content
            echo '<td>'.$tweet->retweet_count.'</td>'; // Retweets
            echo '<td>'.$tweet->favorite_count.'</td>'; // Likes
            
            // Like (note this value doesn't seem to be accurately set)
            if ($tweet->favorited) {
              echo '<td><form method="post" action="index.php">'
                    .'<button type="submit" name="unlike_id" value="'.$tweet->id
                    .'">Unlike This Tweet</button>'
                    .'</form></td>';
            } else {
              echo '<td><form method="post" action="index.php">'
                    .'<button type="submit" name="like_id" value="'.$tweet->id
                    .'">Like This Tweet</button>'
                    .'</form></td>';
            }
            echo '</tr>';
          }
        }
      }
      ?>
      </tbody>
    </table>
  </body>
</html>